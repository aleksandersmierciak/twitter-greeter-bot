# twitter-greeter-bot

A Node.js application working as a Twitter bot. It responds to Twitter users with a custom message when they use a specific hashtag.

How to run the application

1. Clone the repository
1. Create an .env file with the following structure:

   ```plaintext
   Twitter_consumer_key=YOUR_CONSUMER_KEY_GOES_HERE
   Twitter_consumer_secret=YOUR_CONSUMER_SECRET_GOES_HERE
   Twitter_access_token=YOUR_ACCESS_TOKEN_GOES_HERE
   Twitter_access_token_secret=YOUR_ACCESS_TOKEN_SECRET_GOES_HERE

   REDIS_URL=YOUR_REDIS_FULL_URL_GOES_HERE

   Greeter_sought_keywords=YOUR_KEYWORDS_SEPARATED_BY_COMMAS
   Greeter_ignored_user_names=YOUR_IGNORED_USER_NAMES_SEPARATED_BY_COMMAS
   Greeter_response_mode=[log|retweet|reply]
   Greeter_response_content=YOUR_RESPONSE_CONTENT
   ```

1. Run the app with `npm start`.
1. If you define the additional environment variable:

   ```plaintext
   Greeter_response_media=BASE64_ENCODED_MEDIA
   ```

   The application will upload this media to Twitter, log the media id and return.

1. Once you additionally put the media id as an environment variable:

   ```plaintext
   Greeter_response_media_id=ID_OF_MEDIA_UPLOADED_TO_TWITTER
   ```

   And set Greeter_response_mode to 'reply', the application will reply to Tweets with the uploaded media as attachment.
