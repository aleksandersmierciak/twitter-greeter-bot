const Twit = require('twit')
const Redis = require('ioredis')
require('dotenv').config()
const Crypto = require('crypto')
const Https = require('https')

startBot()

function startBot () {
  const twitterClient = initializeTwitterClient()
  if (process.env.Greeter_response_media_url && !process.env.Greeter_response_media_id) {
    getMediaInBase64(process.env.Greeter_response_media_url, function (err, mediaData) {
      if (err) throw err
      uploadMediaToTwitter(twitterClient, mediaData)
    })
  } else {
    reactToTwits(twitterClient)
  }
}

function initializeTwitterClient () {
  const twitterConfiguration = readTwitterClientConfiguration()
  if (!validateTwitterClientConfiguration(twitterConfiguration)) {
    return
  }

  const twitterClient = createTwitterClient(twitterConfiguration)
  return twitterClient
}

function reactToTwits (twitterClient) {
  const ignoredUserNames = process.env.Greeter_ignored_user_names ? process.env.Greeter_ignored_user_names.split(',') : []

  const redisClient = createRedisClient(process.env.REDIS_URL)

  const logTweets = function (tweet) {
    const logTweet = function () {
      console.log(`Logged the tweet with ID '${tweet.id_str}'`)
    }
    twitterCallback(redisClient, ignoredUserNames, tweet, logTweet)
  }

  const retweetTweets = function (tweet) {
    const retweetTweet = function () {
      twitterClient.post('statuses/retweet/:id', { id: tweet.id_str }, function (err, data, response) {
        if (err) throw err
        console.log(`Retweeted the tweet with ID '${tweet.id_str}'`)
      })
    }
    twitterCallback(redisClient, ignoredUserNames, tweet, retweetTweet)
  }

  const replyToTweets = function (tweet) {
    const replyToTweet = function () {
      const responseContent = `@${tweet.user.screen_name} ${process.env.Greeter_response_content}`
      const responseMediaId = process.env.Greeter_response_media_id
      const params = { in_reply_to_status_id: tweet.id_str, status: responseContent, media_ids: [responseMediaId] }
      const replyToTweetResponse = function (err, data, response) {
        if (err) throw err
        console.log(`Replied to the tweet with ID '${tweet.id_str}'`)
      }
      twitterClient.post('statuses/update', params, replyToTweetResponse)
    }
    twitterCallback(redisClient, ignoredUserNames, tweet, replyToTweet)
  }

  console.log('Starting listening on the stream')
  const twitterStream = openTwitterStream(twitterClient)
  const responseMode = process.env.Greeter_response_mode
  switch (responseMode) {
    case 'log':
      console.log('Response mode: logging tweets')
      twitterStream.on('tweet', logTweets)
      break
    case 'retweet':
      console.log('Response mode: retweeting tweets')
      twitterStream.on('tweet', retweetTweets)
      break
    case 'reply':
      console.log('Response mode: replying to tweets')
      twitterStream.on('tweet', replyToTweets)
      break
    default:
      console.err(`Invalid responseMode value: ${responseMode}, check the configuration`)
      break
  }
}

function getMediaInBase64 (mediaUrl, callback) {
  console.log(`Retrieving media from '${mediaUrl}`)
  Https.get(mediaUrl, (response) => {
    response.setEncoding('base64')
    let mediaInBase64 = ''
    response.on('data', (data) => { mediaInBase64 += data })
    response.on('end', () => {
      callback(null, mediaInBase64)
    })
  }).on('error', (err) => {
    console.err(`Got error: ${err.message}`)
    callback(err)
  })
}

function uploadMediaToTwitter (twitterClient, mediaData) {
  console.log('Uploading response media to Twitter')
  twitterClient.post('media/upload', { media_data: mediaData }, function (err, data, response) {
    if (err) throw err
    const mediaId = data.media_id_string
    twitterClient.post('media/metadata/create', { media_id: mediaId, alt_text: { text: 'QR code' } }, function (err, data, response) {
      if (err) throw err
      console.log(`Put '${mediaId}' into the Greeter_response_media_id environment variable and restart the application`)
    })
  })
}

function twitterCallback (redisClient, ignoredUserNames, tweet, responseFunction) {
  const hash = Crypto.createHash('sha256')
  hash.update(tweet.user.id_str)
  var hashedId = hash.digest('hex')
  if (ignoredUserNames.includes(hashedId)) {
    console.log(`The user with ID hashed to '${hashedId}' is ignored`)
  } else {
    redisClient.sismember('greeted_user_names', hashedId, function (err, reply) {
      if (err) throw err
      if (reply === 1) {
        console.log(`The user with ID hashed to '${hashedId}' has already been greeted`)
      } else {
        console.log(`Responding to a Tweet from user with ID hashed to '${hashedId}'`)
        responseFunction()
        redisClient.sadd('greeted_user_names', hashedId)
      }
    })
  }
}

function openTwitterStream (twitterClient) {
  var soughtKeywords = readSoughtKeywords()
  if (!validateSoughtKeywords(soughtKeywords)) {
    return
  }

  return twitterClient.stream('statuses/filter', { track: soughtKeywords })
}

function readTwitterClientConfiguration () {
  return {
    consumer_key: process.env.Twitter_consumer_key,
    consumer_secret: process.env.Twitter_consumer_secret,
    access_token: process.env.Twitter_access_token,
    access_token_secret: process.env.Twitter_access_token_secret
  }
}

function validateTwitterClientConfiguration (twitterConfiguration) {
  if (!twitterConfiguration.consumer_key ||
    !twitterConfiguration.consumer_secret ||
    !twitterConfiguration.access_token ||
    !twitterConfiguration.access_token_secret) {
    console.error('Twitter API configuration was not set, check the configuration')
    return false
  } else {
    return true
  }
}

function createTwitterClient (twitterConfiguration) {
  return new Twit(twitterConfiguration)
}

function createRedisClient (connectionString) {
  return new Redis(connectionString)
}

function readSoughtKeywords () {
  return process.env.Greeter_sought_keywords ? process.env.Greeter_sought_keywords.split(',') : null
}

function validateSoughtKeywords (soughtKeywords) {
  if (!soughtKeywords) {
    console.error('Greeter keywords were not set, check the configuration')
    return false
  } else {
    return true
  }
}
